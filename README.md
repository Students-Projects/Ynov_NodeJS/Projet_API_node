# TP : Todo List

1. Reprendre le TD3
2. Ajouter une table `todos`
3. Créer la resources REST `/todos` (userId, message, createdAt, updatedAt, completedAt)
4. La page d'accueil redirige vers `/todos`
5. Chaque utilisateur ne peut voir que ces propres Todos, chaque utilisateur doit pouvoir cocher une Todo, qui passe alors en

    fin de liste
6. Bonus : Team

      Un utilisateur peut appartenir à une seule Team (ou aucune)
      Un utilisateur appartenant à une team a une option "Voir mes todos / Voir les todos de mon équipe"
      Les todos peuvent être assignées à un utilisateur de la même team
      Les todos peuvent être terminées par n'importe quel utilisateur

[ X ] Sortir les poubelles (par MaChérie, pour moi, complété le 11/10 à 7h30)

